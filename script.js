$(document).ready(function() {
  newQuote();
  $('#new-quote').on('click',newQuote);
});
var i = 0
function newQuote() {
  $.ajax({
    headers: {
      Accept: "application/json",
    },
    url: 'https://gist.githubusercontent.com/camperbot/5a022b72e96c4c9585c32bf6a75f62d9/raw/e3c6895ce42069f0ee7e991229064f167fe8ccdc/quotes.json',
    success: function(response) {
      var parsed = JSON.parse(response);
      var actualQuote = parsed.quotes
      var index = Math.floor(Math.random() * actualQuote.length);
      $('#text').html(actualQuote[index].quote);
      $('#author').text(actualQuote[index].author);
      $('#tweet-quote').attr('href','https://twitter.com/intent/tweet?text="' + actualQuote[index].quote + "("+ actualQuote[index].author +')"');
    }
  });
}
