# Random Quote Machine

This is one of the FreeCodeCamp Challenges for the Front End Libraries Certificate

The Project should be built in [Codepen.io](https://codepen.io/)

In your codepen use just the body of the HTML file. In the Header you can see, which libraries, are used.

## Pen settings

Javascript libs:
- jquery 2.2.2

css libs:
- bootstrap 3.3.6
- font-awesome 4.6.1
